//main.c
#include<stdio.h>
#include"calculate.h"

int main(void)
{
  float Numeral,Result;
  char Operation[4];
  printf("Число: ");
  scanf("%f",&Numeral);
  printf("Операция (+,-,*,/,pow,sqrt,sin,cos,tan,): ");
  scanf("%s",Operation);
  Result=Calculate(Numeral,Operation);
  printf("Результат: %6.2f\n",Result);
  return 0;
}
